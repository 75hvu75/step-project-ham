'use strict';

export const breakingNewsImagesHTML = [
    '<img class="breaking-news-img" src="./images/breaking news/Image1.png" alt="">',
    '<img class="breaking-news-img" src="./images/breaking news/Image2.png" alt="">',
    '<img class="breaking-news-img" src="./images/breaking news/Image3.png" alt="">',
    '<img class="breaking-news-img" src="./images/breaking news/Image4.png" alt="">',
    '<img class="breaking-news-img" src="./images/breaking news/Image5.png" alt="">',
    '<img class="breaking-news-img" src="./images/breaking news/Image6.png" alt="">',
    '<img class="breaking-news-img" src="./images/breaking news/Image7.png" alt="">',
    '<img class="breaking-news-img" src="./images/breaking news/Image8.png" alt="">',
];

const breakingNewsItem = document.querySelectorAll('.breaking-news-item');

export function showBreakingNewsImg(array) {
    for (let i = 0; i < breakingNewsItem.length; i++) {
        const createDivElem = document.createElement('div');
        breakingNewsItem[i].prepend(createDivElem);
        createDivElem.innerHTML = array[i];
    }
}
