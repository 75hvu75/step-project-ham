const dateElements = document.querySelectorAll('.date-block');

export function getCurrentDate() {
    const currentDate = new Date();

    const day = currentDate.getDate();
    const month = currentDate.toLocaleString('en-us', { month: 'short' });

    dateElements.forEach(elem => {
        elem.querySelector('.date-day').textContent = day;
        elem.querySelector('.date-month').textContent = month;
    });
}

getCurrentDate();

setInterval(getCurrentDate, 60000);
