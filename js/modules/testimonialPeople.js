'use strict';

const textArray = [
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo nam sequi, sunt illo assumenda esse inventore voluptates iusto deleniti laboriosam minus sint molestiae quo, quaerat aliquam consectetur dicta quae quam? Aspernatur distinctio, corporis et numquam recusandae quae laboriosam ab accusantium quidem!',
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit quisquam praesentium recusandae optio laboriosam nostrum veritatis a, possimus aperiam hic obcaecati libero expedita suscipit nemo qui fugiat molestiae numquam, voluptas inventore aliquid illo. Odit necessitatibus soluta dignissimos vitae dolores repellendus nisi temporibus blanditiis. Error, ex.',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis commodi itaque ut, adipisci alias iste, unde doloremque officiis dolorem deserunt architecto, pariatur dolore laboriosam reprehenderit saepe! Nam blanditiis rem iste dolorum minus molestiae commodi perferendis adipisci aspernatur. Cumque vero nobis aliquam dolorum libero ducimus nostrum ad facere! Quae, vel adipisci.',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus blanditiis dolor culpa, laboriosam debitis suscipit est nam cumque ut quis sunt quo facere quos mollitia ad ex magnam natus, perspiciatis nostrum distinctio nulla id error odit. Recusandae aperiam magni eius minima, incidunt perferendis!',
];

const names = ['Olivia Williams', 'Ethan Thompson', 'Hasan Ali', 'Sophia Johnson'];

const positions = ['Journalist', 'Photojournalist', 'UX Designer', 'Photo model'];

let countAvatar = 2;

const testimonialAvatarWrapper = document.querySelector('.testimonial-avatar-wrapper');
const testimonialAvatarChoice = document.querySelector('.testimonial-avatar-choice');
const testimonialDescriptionText = document.querySelector('.testimonial-description-text');
const testimonialDescriptionName = document.querySelector('.testimonial-description-name');
const testimonialDescriptionPosition = document.querySelector('.testimonial-description-position');

const arrowLeft = document.querySelector('.arrow-left');
const arrowRight = document.querySelector('.arrow-right');

const animation = [{ transform: 'rotateY(180deg) scale(0)' }, { transform: 'rotateY(360deg) scale(1)' }];
const animationTime = {
    duration: 1000,
    easing: 'linear',
};

export function testimonialChangeAvatar() {

    const avatarElements = testimonialAvatarWrapper.querySelectorAll('.testimonial-avatar');
    
	avatarElements.forEach(elem => {
        elem.classList.remove('testimonial-avatar-active');
    });

    avatarElements[countAvatar].classList.add('testimonial-avatar-active');

    testimonialAvatarChoice.innerHTML = avatarElements[countAvatar].innerHTML;

    const faceSmallImg = testimonialAvatarChoice.querySelector('.testimonial-avatar-img');

    faceSmallImg.classList.remove('testimonial-avatar-img');
    faceSmallImg.classList.add('avatar-choice-img');

    testimonialAvatarChoice.animate(animation, animationTime);

    testimonialDescriptionText.innerText = textArray[countAvatar];
    testimonialDescriptionName.innerText = names[countAvatar];
    testimonialDescriptionPosition.innerText = positions[countAvatar];
}

testimonialAvatarWrapper.addEventListener('click', event => {

	const clickedElement = event.target.closest('.testimonial-avatar');
	
    if (clickedElement) {
        countAvatar = parseInt(clickedElement.dataset.testimonial);
        testimonialChangeAvatar();
    }
});

arrowLeft.addEventListener('click', () => {
    countAvatar = (countAvatar - 1 + names.length) % names.length;
    testimonialChangeAvatar();
});

arrowRight.addEventListener('click', () => {
    countAvatar = (countAvatar + 1) % names.length;
    testimonialChangeAvatar();
});

testimonialChangeAvatar();
