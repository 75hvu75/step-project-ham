'use strict';

const amazingImagesHTML1 = [
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress8.jpg" alt="">',
    '<img data-amazing="web design" src="./images/web design/web-design4.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design8.jpg" alt="">',
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress5.jpg" alt="">',
    '<img data-amazing="landing pages" src="./images/landing page/landing-page2.jpg" alt="">',
    '<img data-amazing="web design" src="./images/web design/web-design1.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design3.jpg" alt="">',
    '<img data-amazing="landing pages" src="./images/landing page/landing-page6.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design9.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design10.jpg" alt="">',
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress2.jpg" alt="">',
    '<img data-amazing="web design" src="./images/web design/web-design6.jpg" alt="">',
];
const amazingImagesHTML2 = [
    '<img data-amazing="landing pages" src="./images/landing page/landing-page7.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design2.jpg" alt="">',
    '<img data-amazing="web design" src="./images/web design/web-design3.jpg" alt="">',
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress1.jpg" alt="">',
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress7.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design5.jpg" alt="n">',
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress3.jpg" alt="">',
    '<img data-amazing="landing pages" src="./images/landing page/landing-page4.jpg" alt="">',
    '<img data-amazing="web design" src="./images/web design/web-design7.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design1.jpg" alt="">',
    '<img data-amazing="web design" src="./images/web design/web-design5.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design11.jpg" alt="">',
];
const amazingImagesHTML3 = [
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design4.jpg" alt="">',
    '<img data-amazing="landing pages" src="./images/landing page/landing-page5.jpg" alt="">',
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress10.jpg" alt="">',
    '<img data-amazing="landing pages" src="./images/landing page/landing-page3.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design12.jpg" alt="">',
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress6.jpg" alt="">',
    '<img data-amazing="web design" src="./images/web design/web-design2.jpg" alt="">',
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress9.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design6.jpg" alt="">',
    '<img data-amazing="wordpress" src="./images/wordpress/wordpress4.jpg" alt="">',
    '<img data-amazing="graphic design" src="./images/graphic design/graphic-design7.jpg" alt="">',
    '<img data-amazing="landing pages" src="./images/landing page/landing-page1.jpg" alt="">',
];

export const amazingImagesHTML = [amazingImagesHTML1, amazingImagesHTML2, amazingImagesHTML3];

// Сортування вкладок і картинок для "Our amazing work" .

const amazingTabsList = document.querySelector('.amazing-tabs-list');
const amazingContent = document.querySelector('.amazing-content');
let targetAmazingTabsList;

function switchAmazingImg(target) {
    [...amazingTabsList.children].forEach(item => {
        item.classList.remove('amazing-active');
    });

    target.classList.add('amazing-active');

    [...amazingContent.children].forEach(elem => {
        if ([...elem.children][0].dataset.amazing === target.dataset.amazing) {
            elem.classList.add('amazing-content-item-active');
        } else {
            elem.classList.remove('amazing-content-item-active');
        }
        if (target.dataset.amazing === 'all') {
            elem.classList.add('amazing-content-item-active');
        }
    });
}

amazingTabsList.addEventListener('click', elem => {
    targetAmazingTabsList = elem.target;
    switchAmazingImg(targetAmazingTabsList);
});

// Створення картинок для "Our Amazing Work"

let createDivElem;
let amazingCategory;

export function showAmazingImg(array) {
    array.forEach(elem => {
        createDivElem = document.createElement('div');
        createDivElem.classList.add('amazing-content-item', 'amazing-content-item-active');
        amazingContent.append(createDivElem);

        if (elem.indexOf('data-amazing="graphic design"') === 5) {
            amazingCategory = 'Graphic design';
        }
        if (elem.indexOf('data-amazing="web design"') === 5) {
            amazingCategory = 'Web design';
        }
        if (elem.indexOf('data-amazing="landing pages"') === 5) {
            amazingCategory = 'Landing pages';
        }
        if (elem.indexOf('data-amazing="wordpress"') === 5) {
            amazingCategory = 'Wordpress';
        }

        createDivElem.innerHTML =
            elem +
            `<div class="amazing-content-revers">
				<div class="amazing-content-icons">
					<svg class="chain-icon" width="43" height="42" viewBox="0 0 43 42" fill="none" xmlns="http://www.w3.org/2000/svg">
						<rect x="1" y="1" width="41" height="40" rx="20" stroke="var(--primary-color)" />
						<path fill-rule="evenodd" clip-rule="evenodd" d="M26.913 16.7282L25.0948 14.8913C24.2902 14.0809 22.983 14.0759 22.1768 14.8826L20.1592 16.8926C19.3516 17.6989 19.3481 19.0103 20.1505 19.8207L21.3035 18.689C21.1867 18.3284 21.3304 17.9153 21.6159 17.6295L22.8995 16.3519C23.3061 15.9462 23.9584 15.9491 24.3595 16.3543L25.4513 17.458C25.8527 17.8628 25.851 18.5171 25.4469 18.9232L24.1634 20.2024C23.8918 20.473 23.446 20.6217 23.1002 20.5263L21.9709 21.6589C22.7745 22.4718 24.0803 22.4747 24.8889 21.6684L26.9039 19.6592C27.7141 18.8525 27.7166 17.5398 26.913 16.7282ZM19.5261 24.0918C19.6219 24.4441 19.4685 24.8997 19.1909 25.1777L17.9922 26.3752C17.5807 26.7845 16.9159 26.7833 16.5067 26.369L15.3929 25.2475C14.9846 24.8349 14.9873 24.1633 15.3982 23.7547L16.598 22.5577C16.8903 22.2661 17.3104 22.1202 17.6771 22.2438L18.8335 21.0715C18.0149 20.2462 16.6825 20.2421 15.8606 21.0632L13.9152 23.0042C13.0922 23.8266 13.0883 25.1629 13.9065 25.9886L15.7581 27.8618C16.5759 28.6846 17.9072 28.6912 18.7311 27.8701L20.6765 25.9287C21.4985 25.1054 21.5024 23.7717 20.6855 22.9443L19.5261 24.0918ZM19.2578 23.5631C18.9801 23.8419 18.5342 23.8411 18.2618 23.5581C17.9879 23.2743 17.9901 22.8204 18.2661 22.5399L21.5907 19.1611C21.8668 18.8823 22.3117 18.8831 22.5851 19.164C22.8604 19.4457 22.8587 19.9009 22.5817 20.183L19.2578 23.5631Z" fill="var(--primary-color)"/>
					</svg>

					<svg class="stop-icon" width="43" height="43" viewBox="0 0 43 43" fill="none" xmlns="http://www.w3.org/2000/svg">
						<rect x="1" y="2" width="41" height="40" rx="20" stroke="var(--primary-color)" />
						<path fill-rule="evenodd" clip-rule="evenodd" d="M21.5973 1.99795C32.8653 1.99795 41.9999 10.9523 41.9999 21.9979C41.9999 33.0432 32.8653 41.9979 21.5973 41.9979C10.3292 41.9979 1.19463 33.0432 1.19463 21.9979C1.19463 10.9523 10.3292 1.99795 21.5973 1.99795Z" fill="none"/>
						<rect x="15" y="17" width="12" height="11" fill="var(--primary-color)" />
					</svg>
				</div>
				<p class="amazing-content-revers-title">CREATIVE DESIGN</p>
				<p class="amazing-content-revers-description">${amazingCategory}</p>
			</div>
		`;
    });
}


//Додавання картинок для "Our Amazing Work""

let amazingCount = 1;
const amazingButton = document.querySelector('.amazing-button');

amazingButton.addEventListener('click', elem => {
    if (amazingCount < amazingImagesHTML.length - 1) {
        showAmazingImg(amazingImagesHTML[amazingCount]);
    } else {
        showAmazingImg(amazingImagesHTML[amazingCount]);
        amazingButton.classList.add('amazing-button-deactive');
    }

    amazingCount++;

    if (!!targetAmazingTabsList) {
        switchAmazingImg(targetAmazingTabsList);
    }
});