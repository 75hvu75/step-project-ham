'use strict';

import { getCurrentDate } from "./modules/currentDate.js";
import { testimonialChangeAvatar } from './modules/testimonialPeople.js';
import { showBreakingNewsImg, breakingNewsImagesHTML } from './modules/breakingNews.js';
import { showAmazingImg, amazingImagesHTML } from './modules/tabAmazing.js';
import { showServicesTabSwitch } from './modules/tabServises.js';

showServicesTabSwitch();
showAmazingImg(amazingImagesHTML[0]);
showBreakingNewsImg(breakingNewsImagesHTML);
testimonialChangeAvatar();
getCurrentDate();